Runescape for Unofficial GNU/Linux
=========================================

[Runescape](https://runescape.com) is a massively multiplayer online role-playing game 
created by [Jagex Ltd](https://jagex.com).

**Script (executable)**
-----------------------

This script provide you with a simple and easy way to play RuneScape
on GNU/Linux.

You can work on all GNU/Linux distributions, the script is sufficient
to release the JVM with the game client runescape to play regularly.

At first a graphical dialog box (zenity) appears where can choose the language
that will start playing: **English, French, Spanish, Portuguese or German**.

**Dependencies**
----------------

To use the script, you will need to have the following dependencies installed:

- 7zip

- OpenJDK

- Zenity

**Installation dependency: Debian/Ubuntu**
------------------------------------------

    # apt install default-jre make p7zip-full zenity

When all these dependencies have installed, simply run the script.

Next you need to compile this release.
    
**Make Install and Make Uninstall**
-----------------------------------

    # make install
    
    # make uninstall

License
=======

Copyright (C) 2016-2017 Carlos Donizete Froes [a.k.a coringao]

Use of this script is governed by a BSD 2-clause license that can be found in the LICENSE file.

Source code and contact info at https://github.com/coringao/runescape
